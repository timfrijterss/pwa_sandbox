import {Component} from '@angular/core';
import {ScoreFlock, ScoreFlockDB} from '../model/ScoreFlock';
import {HttpClient} from '@angular/common/http';
import {reject} from 'q';
import {from, Observable} from 'rxjs';

@Component({
  selector: 'app-persist-module',
  templateUrl: './pwa-module.component.html',
  styleUrls: ['./pwa-module.component.css']
})
export class PwaModuleComponent {

  persistenceId: number;
  score: number;
  private db: ScoreFlockDB;

  constructor(private http: HttpClient) {
    this.db = new ScoreFlockDB();
    Notification.requestPermission((result) => {
      if (result !== 'granted') {
        return reject(Error('Denied notification permission'));
      }
    });
    if (navigator.storage && navigator.storage.persist()) {
      navigator.storage.persist().then(() => {
        console.log('persistent storage granted');
      });
    }
  }

  test(): void {
    const endpoint = 'https://www.mocky.io/v2/5d3070ca320000045a2044da';
    const scoreFlock: ScoreFlock = {persistenceId: this.persistenceId, score: this.score};
    this.http.post<ScoreFlock>(endpoint, scoreFlock).subscribe(
      (data) => {
        console.log('Data posted successfully: ' + data);
      },
      (error) => {
        console.log('[CLIENT] call failed' + error);
        this.putInDexie(scoreFlock.persistenceId, endpoint, scoreFlock).subscribe(() => {
          console.log('[CLIENT] indexedDb updated');
          this.requestSync('sync-post');
        });
      }
    );
  }

  requestSync(requestType): void {
    console.log('[CLIENT] trying to get service worker');
    navigator.serviceWorker.ready.then((swRegistration) => {
      console.log('[CLIENT] serviceworker active trying to sync...');
      // feature detection for browsers who do not support background syncing
      if (swRegistration.sync) {
        swRegistration.sync.register(requestType).then(() => {
          console.log('[CLIENT] sync is registered');
        });
      } else {
        this.addOnlineEventListener();
      }
    });
  }

  private addOnlineEventListener() {
    window.addEventListener('online', () => {
      navigator.serviceWorker.controller.postMessage('sync');
    });
  }

  private putInDexie(persistenceId, endpoint, data): Observable<string> {
    return from(this.db.request.put({persistenceId, endpoint, data, dirty: 1}));
  }

}
