import Dexie from 'dexie';

export class ScoreFlockDB extends Dexie {
  request: Dexie.Table<ScoreFlockRequest, string>;

  constructor() {
    super('pwa-offline-tasks');
    this.version(1).stores({
      request: 'persistenceId, dirty'
    });
  }
}

export interface ScoreFlock {
  persistenceId: number;
  score: number;
}

interface ScoreFlockRequest {
  persistenceId: number; // repeated this property because we use this as key.
  endpoint: string;
  data: ScoreFlock;
  dirty: number;
}
