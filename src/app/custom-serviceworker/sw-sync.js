/* eslint-env serviceworker */
importScripts('./dexie.min.js');

// NOTE This is expermintal and only works in chrome / chrome mobile...

/**
 * @function syncSW
 * @param event - event with a tag to find out which method should be called
 */
self.addEventListener('sync', event => {
  console.log('SW sync received');
  return event.waitUntil(doRequest());
});

self.addEventListener('message', event => {
  console.log('SW message received', event);
  if (event.data === 'sync') {
    return doRequest();
  } else {
    console.log('other message:' + event.data)
  }
});

/**
 * @function doRequest
 * Executes a list of offline tasks when the service worker detects connectivity is back
 **/
async function doRequest() {
  const tasks = [];
  const db = await new Dexie('pwa-offline-tasks').open();
  const requestTable = db.table('request');
  await requestTable
    .where('dirty')
    .equals(1)
    .each(request => {
      tasks.push(
        {
          endpoint: request.endpoint, body: {
            method: 'POST',
            headers: {
              'ngsw-bypass': true, // this fetch is always a post, ignore ngsw caching
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(request.data)
          }
        }
      )
    });

  Promise.all(tasks.map(task => fetch(task.endpoint, task.body)))
    .then(async function (data) {
      await pushNotification();
      // MUDO We could notify the client here with a push notification that the sync succeeded
      return requestTable.clear()
    })
    .catch(function (err) {
      console.error('[SW] Offline tasks sync failed:', err) // MUDO How are we going to handle errors outside the scope of the browser?
    })
}

function pushNotification() {
  var options = {
    body: 'This notification was generated because a call was made!',
    icon: 'assets/icon-72x72.png',
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      primaryKey: '2'
    },
    actions: [
      {
        action: 'explore', title: 'Custom button',
        icon: 'assets/icon72x72.png'
      },
      {
        action: 'close', title: 'Close',
        icon: 'assets/icon72x72.png'
      },
    ]
  };
  return self.registration.showNotification('Hello world!', options)
}
