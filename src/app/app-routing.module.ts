import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Module2Component} from './module2/module2.component';
import {PwaModuleComponent} from './pwa-module/pwa-module.component';

const routes: Routes = [
  {path: 'module2', component: Module2Component},
  {path: 'pwa', component: PwaModuleComponent},
  // {
  //   path: '',
  //   component: PwaModuleComponent,
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
